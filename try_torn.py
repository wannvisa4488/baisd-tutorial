def changeMoney(coin):
    dten = coin // 10
    c1 = coin % 10
    dfive = c1 // 5
    c2 = c1 % 5
    dtwo = c2 // 2
    c3 = c2 % 2
    done = c3 // 1
    return f"use 10 : {dten} \nuse 5 : {dfive} \nuse 2 : {dtwo} \nuse 1 : {done}"


if __name__ == "__main__":
    c = int(input("Enter Coin : "))
    d = changeMoney(c)
    print(d)
